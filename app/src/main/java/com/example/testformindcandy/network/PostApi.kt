package com.example.testformindcandy.network

import com.example.testformindcandy.model.commentlist.CommentResponse
import com.example.testformindcandy.model.postlist.PostsResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostApi {

    /**
     * Get the list of the pots from the API
     */
    @GET("/.json")
    fun getPosts(
        @Query("after") after: String = ""
    ): Observable<PostsResponse>

    /**
     * Get the list of the pots from the API
     */
    @GET("{permanlink}.json?depth=2")
    fun getComments(
        @Path("permanlink", encoded = true) id: String
    ): Observable<List<CommentResponse>>
}