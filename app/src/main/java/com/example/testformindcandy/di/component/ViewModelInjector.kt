package com.example.testformindcandy.di.component

import com.example.testformindcandy.di.module.NetworkModule
import com.example.testformindcandy.ui.commentlist.CommentListViewModel
import com.example.testformindcandy.ui.postlist.PostListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param postListViewModel PostListViewModel in which to inject the dependencies
     */
    fun inject(postListViewModel: PostListViewModel)

    fun inject(commentListViewModel : CommentListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}