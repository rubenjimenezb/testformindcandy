package com.example.testformindcandy.base

import androidx.lifecycle.ViewModel
import com.example.testformindcandy.di.component.DaggerViewModelInjector
import com.example.testformindcandy.di.component.ViewModelInjector
import com.example.testformindcandy.di.module.NetworkModule
import com.example.testformindcandy.ui.commentlist.CommentListViewModel
import com.example.testformindcandy.ui.postlist.PostListViewModel

abstract class BaseViewModel: ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)
            is CommentListViewModel -> injector.inject(this)

        }
    }
}