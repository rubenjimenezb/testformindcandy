package com.example.testformindcandy.ui.postlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testformindcandy.R
import com.example.testformindcandy.databinding.ItemPostBinding
import com.example.testformindcandy.model.postlist.PostItem
import com.example.testformindcandy.utils.navigation.NavigationImpl

class PostListAdapter: RecyclerView.Adapter<PostListAdapter.ViewHolder>() {
    private var postList = ArrayList<PostItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_post, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    fun updatePostList(postList:List<PostItem>){
        this.postList.addAll(postList)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemPostBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = PostViewModel()
        private var handlers : PostHandlers? = null

        fun bind(post: PostItem){
            viewModel.bind(post)
            binding.viewModel = viewModel
            handlers = PostHandlers(post)
            binding.handlers = handlers
        }
    }

    class PostHandlers(private val postItem: PostItem) {
        fun onClickPost(view: View) {
            NavigationImpl().openUrlInExternalNavigator(view.context, postItem.data?.url ?: "")
        }

        fun onClickComments(view: View) {
            NavigationImpl().goPostCommentsScreen(view.context, postItem)
        }
    }
}