package com.example.testformindcandy.ui.commentlist

import androidx.lifecycle.MutableLiveData
import com.example.testformindcandy.base.BaseViewModel

class CommentViewModel: BaseViewModel() {
    private val commentTitle = MutableLiveData<String>()

    fun bind(comment: String){
        commentTitle.value = comment
    }

    fun getCommentTitle(): MutableLiveData<String> {
        return commentTitle
    }
}