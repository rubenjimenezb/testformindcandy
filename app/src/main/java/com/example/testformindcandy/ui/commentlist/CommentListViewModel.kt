package com.example.testformindcandy.ui.commentlist

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.testformindcandy.R
import com.example.testformindcandy.base.BaseViewModel
import com.example.testformindcandy.model.commentlist.CommentResponse
import com.example.testformindcandy.model.postlist.PostItem
import com.example.testformindcandy.network.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CommentListViewModel : BaseViewModel() {
    @Inject
    lateinit var postApi: PostApi

    private lateinit var subscription: Disposable

    private lateinit var postItem: PostItem

    val postTitle: MutableLiveData<String> = MutableLiveData()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadComments(postItem) }

    val commentListAdapter: CommentListAdapter = CommentListAdapter()

    fun loadComments(postItem: PostItem) {
        this.postItem = postItem

        //Fill the title
        postTitle.value = postItem.data?.title ?: ""

        subscription = postApi.getComments(postItem.data?.permalink ?: "")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveCommentListStart() }
            .doOnTerminate { onRetrieveCommentListFinish() }
            .subscribe(
                { result -> onRetrieveCommentListSuccess(result) },
                { error -> onRetrieveCommentListError(error) }
            )
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onRetrieveCommentListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveCommentListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveCommentListSuccess(result: List<CommentResponse>) {
        //Difficult to access the list of comments
        val commentList = result.last().data?.children?.map { childrenItem ->
            "${childrenItem?.data?.author ?: ""}: ${childrenItem?.data?.body ?: ""}"
        } ?: emptyList()

        Log.d("CommentListViewModel", commentList.toString())
        commentListAdapter.updateCommentList(commentList)
    }

    private fun onRetrieveCommentListError(error: Throwable) {
        Log.d("CommentListViewModel", error.toString())
        errorMessage.value = R.string.comment_error
    }
}