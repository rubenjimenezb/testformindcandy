package com.example.testformindcandy.ui.commentlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testformindcandy.R
import com.example.testformindcandy.databinding.ItemCommentBinding

class CommentListAdapter : RecyclerView.Adapter<CommentListAdapter.ViewHolder>() {
    private lateinit var commentList: List<String>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemCommentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_comment, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(commentList[position])
    }

    override fun getItemCount(): Int {
        return if(::commentList.isInitialized) commentList.size else 0
    }

    fun updateCommentList(commentList:List<String>){
        this.commentList = commentList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemCommentBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = CommentViewModel()

        fun bind(comment: String){
            viewModel.bind(comment)
            binding.viewModel = viewModel
        }
    }
}