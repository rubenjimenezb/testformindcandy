package com.example.testformindcandy.ui.commentlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testformindcandy.R
import com.example.testformindcandy.databinding.ActivityCommentListBinding
import com.example.testformindcandy.model.postlist.PostItem
import com.example.testformindcandy.utils.navigation.NavigationConstants
import com.google.android.material.snackbar.Snackbar

class CommentListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCommentListBinding
    private lateinit var viewModel: CommentListViewModel

    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val postItem = intent.getParcelableExtra(NavigationConstants.POST_ITEM_KEY) as PostItem

        binding = DataBindingUtil.setContentView(this, R.layout.activity_comment_list)
        viewModel = ViewModelProviders.of(this).get(CommentListViewModel::class.java)

        viewModel.loadComments(postItem)

        binding.commentList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })
        binding.viewModel = viewModel
    }

    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }
}
