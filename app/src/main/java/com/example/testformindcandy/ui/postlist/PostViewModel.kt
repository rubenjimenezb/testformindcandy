package com.example.testformindcandy.ui.postlist

import androidx.lifecycle.MutableLiveData
import com.example.testformindcandy.base.BaseViewModel
import com.example.testformindcandy.model.postlist.PostItem

class PostViewModel: BaseViewModel() {
    private val postTitle = MutableLiveData<String>()
    private val postUrlImage = MutableLiveData<String>()

    fun bind(postItem: PostItem){
        postTitle.value = postItem.data?.title ?: ""
        postUrlImage.value = postItem.data?.thumbnail ?: ""
    }

    fun getPostTitle():MutableLiveData<String>{
        return postTitle
    }

    fun getPostUrlImage():MutableLiveData<String>{
        return postUrlImage
    }
}