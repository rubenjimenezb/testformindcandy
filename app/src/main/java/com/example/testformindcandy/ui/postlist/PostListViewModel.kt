package com.example.testformindcandy.ui.postlist

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.testformindcandy.R
import com.example.testformindcandy.base.BaseViewModel
import com.example.testformindcandy.model.postlist.PostsResponse
import com.example.testformindcandy.network.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostListViewModel: BaseViewModel() {
    @Inject
    lateinit var postApi: PostApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts(nextPostsCode) }

    private var nextPostsCode : String = ""

    val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener(){
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (!recyclerView.canScrollVertically(1)) {
                if(nextPostsCode != ""){
                    loadPosts(nextPostsCode)
                }
            }
        }
    }

    val postListAdapter: PostListAdapter = PostListAdapter()

    init{
        loadPosts()
    }

    private fun loadPosts(nextPostsCode: String = ""){
        subscription = postApi.getPosts(nextPostsCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                { result -> onRetrievePostListSuccess(result) },
                { error -> onRetrievePostListError(error) }
            )
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(result: PostsResponse) {
        nextPostsCode = result.data?.after ?: ""
        val postItemList = result.data?.children ?: emptyList()
        postListAdapter.updatePostList(postItemList)
        Log.d("PostListViewModel", "onRetrievePostListSuccess: $postItemList")
    }

    private fun onRetrievePostListError(error: Throwable) {
        Log.d("PostListViewModel", error.toString())
        errorMessage.value = R.string.post_error
    }
}