package com.example.testformindcandy.utils.imageloader

import android.content.Context
import android.widget.ImageView

interface ImageLoader {
    fun load(context: Context, i: ImageView, res: Int)
    fun load(context: Context, i: ImageView, url: String)
}