package com.example.testformindcandy.utils.imageloader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.example.testformindcandy.R
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions



class ImageLoaderImpl : ImageLoader {
    /**
     * Load image from resource
     */
    override fun load(context: Context, i: ImageView, res: Int) {
        val options = RequestOptions()
            .fitCenter()
            .placeholder(R.drawable.ic_error_black_24dp)
            .error(R.drawable.ic_error_black_24dp)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)

        Glide.with(context)
            .load(res)
            .apply(options)
            .into(i)
    }

    /**
     * Load image from url (internet)
     */
    override fun load(context: Context, i: ImageView, url: String) {
        val options = RequestOptions()
            .fitCenter()
            .placeholder(R.drawable.ic_error_black_24dp)
            .error(R.drawable.ic_error_black_24dp)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)

        Glide.with(context)
            .load(url)
            .apply(options)
            .into(i)
    }
}