package com.example.testformindcandy.utils.navigation

import android.content.Context
import com.example.testformindcandy.model.postlist.PostItem

interface Navigation {
    fun openUrlInExternalNavigator(context: Context, url: String)

    fun goPostCommentsScreen(context: Context, postItem: PostItem)
}