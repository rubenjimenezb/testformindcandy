package com.example.testformindcandy.utils.navigation

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.example.testformindcandy.model.postlist.PostItem
import com.example.testformindcandy.ui.commentlist.CommentListActivity


class NavigationImpl : Navigation {

    override fun goPostCommentsScreen(context: Context, postItem: PostItem) {
        //Go to comments screen
        val intent = Intent(context, CommentListActivity::class.java)
        intent.putExtra(NavigationConstants.POST_ITEM_KEY, postItem)
        context.startActivity(intent)
    }

    override fun openUrlInExternalNavigator(context: Context, url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }
}