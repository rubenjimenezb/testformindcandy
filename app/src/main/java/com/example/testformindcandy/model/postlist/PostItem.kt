package com.example.testformindcandy.model.postlist

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PostItem(

    @field:SerializedName("data")
	val data: Data? = null,

    @field:SerializedName("kind")
	val kind: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readParcelable(Data::class.java.classLoader),
		parcel.readString()
	)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeParcelable(data, flags)
		parcel.writeString(kind)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<PostItem> {
		override fun createFromParcel(parcel: Parcel): PostItem {
			return PostItem(parcel)
		}

		override fun newArray(size: Int): Array<PostItem?> {
			return arrayOfNulls(size)
		}
	}
}