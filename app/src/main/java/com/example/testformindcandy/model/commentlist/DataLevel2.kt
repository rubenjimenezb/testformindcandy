package com.example.testformindcandy.model.commentlist

import com.google.gson.annotations.SerializedName

data class DataLevel2(

    @field:SerializedName("body")
    val body: String? = null,

    @field:SerializedName("author")
    val author: String? = null
)