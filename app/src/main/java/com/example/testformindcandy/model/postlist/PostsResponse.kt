package com.example.testformindcandy.model.postlist

import com.google.gson.annotations.SerializedName

data class PostsResponse(

    @field:SerializedName("data")
	val data: Data? = null,

    @field:SerializedName("kind")
	val kind: String? = null
)