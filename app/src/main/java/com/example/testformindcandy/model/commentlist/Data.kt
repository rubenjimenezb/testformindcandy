package com.example.testformindcandy.model.commentlist

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("modhash")
	val modhash: String? = null,

	@field:SerializedName("children")
	val children: List<ChildrenItem?>? = null,

	@field:SerializedName("dist")
	val dist: Int? = null,

	@field:SerializedName("saved")
	val saved: Boolean? = null,

	@field:SerializedName("hide_score")
	val hideScore: Boolean? = null,

	@field:SerializedName("total_awards_received")
	val totalAwardsReceived: Int? = null,

	@field:SerializedName("subreddit_id")
	val subredditId: String? = null,

	@field:SerializedName("num_comments")
	val numComments: Int? = null,

	@field:SerializedName("score")
	val score: Int? = null,

	@field:SerializedName("whitelist_status")
	val whitelistStatus: String? = null,

	@field:SerializedName("spoiler")
	val spoiler: Boolean? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("created_utc")
	val createdUtc: Double? = null,

	@field:SerializedName("allow_live_comments")
	val allowLiveComments: Boolean? = null,

	@field:SerializedName("domain")
	val domain: String? = null,

	@field:SerializedName("num_duplicates")
	val numDuplicates: Int? = null,

	@field:SerializedName("no_follow")
	val noFollow: Boolean? = null,

	@field:SerializedName("ups")
	val ups: Int? = null,

	@field:SerializedName("author_flair_type")
	val authorFlairType: String? = null,

	@field:SerializedName("permalink")
	val permalink: String? = null,

	@field:SerializedName("wls")
	val wls: Int? = null,

	@field:SerializedName("gilded")
	val gilded: Int? = null,

	@field:SerializedName("send_replies")
	val sendReplies: Boolean? = null,

	@field:SerializedName("archived")
	val archived: Boolean? = null,

	@field:SerializedName("can_mod_post")
	val canModPost: Boolean? = null,

	@field:SerializedName("is_self")
	val isSelf: Boolean? = null,

	@field:SerializedName("author_fullname")
	val authorFullname: String? = null,

	@field:SerializedName("selftext")
	val selftext: String? = null,

	@field:SerializedName("upvote_ratio")
	val upvoteRatio: Double? = null,


	@field:SerializedName("is_crosspostable")
	val isCrosspostable: Boolean? = null,

	@field:SerializedName("clicked")
	val clicked: Boolean? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("parent_whitelist_status")
	val parentWhitelistStatus: String? = null,

	@field:SerializedName("stickied")
	val stickied: Boolean? = null,

	@field:SerializedName("quarantine")
	val quarantine: Boolean? = null,

	@field:SerializedName("link_flair_background_color")
	val linkFlairBackgroundColor: String? = null,

	@field:SerializedName("over_18")
	val over18: Boolean? = null,

	@field:SerializedName("subreddit")
	val subreddit: String? = null,

	@field:SerializedName("can_gild")
	val canGild: Boolean? = null,

	@field:SerializedName("is_robot_indexable")
	val isRobotIndexable: Boolean? = null,

	@field:SerializedName("post_hint")
	val postHint: String? = null,

	@field:SerializedName("locked")
	val locked: Boolean? = null,

	@field:SerializedName("thumbnail")
	val thumbnail: String? = null,

	@field:SerializedName("downs")
	val downs: Int? = null,

	@field:SerializedName("created")
	val created: Double? = null,

	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("link_flair_text_color")
	val linkFlairTextColor: String? = null,

	@field:SerializedName("is_video")
	val isVideo: Boolean? = null,

	@field:SerializedName("is_original_content")
	val isOriginalContent: Boolean? = null,

	@field:SerializedName("subreddit_name_prefixed")
	val subredditNamePrefixed: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("media_only")
	val mediaOnly: Boolean? = null,

	@field:SerializedName("pinned")
	val pinned: Boolean? = null,

	@field:SerializedName("hidden")
	val hidden: Boolean? = null,

	@field:SerializedName("author_patreon_flair")
	val authorPatreonFlair: Boolean? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("num_crossposts")
	val numCrossposts: Int? = null,

	@field:SerializedName("thumbnail_width")
	val thumbnailWidth: Int? = null,

	@field:SerializedName("subreddit_type")
	val subredditType: String? = null,

	@field:SerializedName("is_meta")
	val isMeta: Boolean? = null,

	@field:SerializedName("subreddit_subscribers")
	val subredditSubscribers: Int? = null,

	@field:SerializedName("thumbnail_height")
	val thumbnailHeight: Int? = null,

	@field:SerializedName("link_flair_type")
	val linkFlairType: String? = null,

	@field:SerializedName("visited")
	val visited: Boolean? = null,

	@field:SerializedName("pwls")
	val pwls: Int? = null,

	@field:SerializedName("contest_mode")
	val contestMode: Boolean? = null,

	@field:SerializedName("is_reddit_media_domain")
	val isRedditMediaDomain: Boolean? = null
)