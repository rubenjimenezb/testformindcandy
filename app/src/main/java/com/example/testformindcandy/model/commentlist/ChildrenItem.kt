package com.example.testformindcandy.model.commentlist

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ChildrenItem(

	@field:SerializedName("data")
	val data: DataLevel2? = null,

	@field:SerializedName("kind")
	val kind: String? = null
)